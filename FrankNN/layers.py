import numpy as np
from .activations import *


class Layer:

    def __init__(self, shape):
        self.shape = shape
        self.w = self.b = self.x = None
        self.dw = self.db = self.dx = None
        self.cache_x = {}  # cache for backprop
        self.cache_w = {}  # cache for optizer
        self.cache_b = {}  # cache for optizer

    def forward(self, x):
        pass

    def backward(self, dout):
        pass

    def __str__(self):
        return 'Layer of shape ' + str((None,)+self.shape)

    def update_weights(self, update_rule):
        if self.w is not None:
            update_rule.update(self.w, self.dw, self.cache_w)
        if self.b is not None:
            update_rule.update(self.b, self.db, self.cache_b)


class Input(Layer):

    def __init__(self, shape):
        Layer.__init__(self, shape)

    def forward(self, x):
        return x


class Dense(Layer):

    def __init__(self, input, channels, activation='linear'):
        Layer.__init__(self, (channels,))
        self.input = input
        self.w = np.clip(np.random.normal(0, 0.1,
                         size=input.shape+(channels,)), -0.2, 0.2)
        self.b = np.ones(channels) * 0.1

        if activation == 'linear':
            self.activation = Linear()
        elif activation == 'relu':
            self.activation = Relu()
        elif activation == 'sigmoid':
            self.activation = Sigmoid()
        else:
            error('Unknown activation function')

    def forward(self, x):
        out = x.dot(self.w) + self.b
        self.cache_x = x
        return self.activation.forward(out)

    def backward(self, dout):
        dout = self.activation.backward(dout)
        self.dw = self.cache_x.T.dot(dout)
        self.db = np.sum(dout, axis=0)
        dxp = dout.dot(self.w.T)
        dx = dxp.reshape(self.cache_x.shape)
        return dx
