from sklearn import datasets
import numpy as np
import FrankNN as NN
import time

# Feed all data in each iteration
batch_size = 150
# Repeat 30k times
epochs = 30000

# Set a random seet for reproducibility.
np.random.seed(42)

# Load the Iris dataset
iris = datasets.load_iris()
X = iris.data
y = iris.target

# Shuffle the data
inds = np.arange(len(X))
np.random.shuffle(inds)
X = X[inds, :]
y = y[inds]

# Split the data into train, validation and test set
n_train = 100
n_validate = 25
n_test = 25
x_train = X[0:n_train]
y_train = y[0:n_train]
x_validate = X[n_train:n_train+n_validate]
y_validate = y[n_train:n_train+n_validate]
x_test = X[-n_test:]
y_test = y[-n_test:]

# Assemble the model. A simple dataset like iris doesn't need a very deep model.
# What worked well is an increasing number of channels with increasing depth.
model = NN.Model()
model.add_layer(NN.Input(X.shape[1:]))
model.add_layer(NN.Dense(model.get_output_layer(), 16, activation='sigmoid'))
model.add_layer(NN.Dense(model.get_output_layer(), 32, activation='sigmoid'))
model.add_layer(NN.Dense(model.get_output_layer(), 64, activation='sigmoid'))
model.add_layer(NN.Dense(model.get_output_layer(), 3))

# Specify loss and update rule. I chose to use softmax cross entropy loss and
# the adagrad update rule.
model.set_loss(NN.Softmax_cross_entropy_loss())
model.set_update_rule(NN.Adagrad(learning_rate=1e-3))

# Start training.
start = time.time()
model.fit(X, y,
          x_validate=x_validate, y_validate=y_validate,
          batch_size=batch_size, epochs=epochs)
end = time.time()
print('Training took {:.0f} minutes'.format((end - start)/60))

# Calculate final model accuracy on test set. With the above choice of model
# and hyperparams, this leads to a 100% test accuracy.
y_test_pred = model.predict(x_test)
y_test_pred = NN.reverse_one_hot_encode(y_test_pred)
accuracy = np.sum(y_test_pred == y_test) / y_test.shape[0]
print('Final test set accuracy = {:.2f}%'.format(accuracy*100))
